const initialState = {
    userId: -1,
    user: {},
    cardsToSell: [],
    socket: undefined,
}

function globalReducer(state = initialState, action) {
    let nextState
    switch (action.type) {

        case 'SET_USERID':
            nextState = {
                ...state,
                userId: action.value
            }
            return nextState || state

        case 'SET_USER':
            nextState = {
                ...state,
                user: action.value
            }
            return nextState || state

        case 'SET_CARDS_TO_SELL':
            nextState = {
                ...state,
                cardsToSell: action.value
            }
            return nextState || state

        case 'SET_SOCKET':
            nextState = {
                ...state,
                socket: action.value
            }
            return nextState || state

        default:
            return state
    }
}

export default globalReducer;
  