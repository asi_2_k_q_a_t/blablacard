export function _setUserId(props, value) {
    const action = { type: "SET_USERID", value: value }
    props.dispatch(action)
}

export function _setUser(props, value) {
    const action = { type: "SET_USER", value: value }
    props.dispatch(action)
}

export function _setCardsToSell(props, value) {
    const action = { type: "SET_CARDS_TO_SELL", value: value }
    props.dispatch(action)
}

export function _setSocket(props, value) {
    const action = { type: "SET_SOCKET", value: value }
    props.dispatch(action)
}