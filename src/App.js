// Public imports
import React from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import globalReducer from './Reducers/globalReducer';

// Custom components
import Main from './Components/Main'

const store = createStore(globalReducer);

class App extends React.Component {

  render(){
    return(
      <Provider store={store}>
        <Main/>
      </Provider>
    )
  }
} 

export default App
