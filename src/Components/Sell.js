// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import { connect } from 'react-redux'
import { Button, Table, TableBody, TableCell, TableHead, TableRow, Typography, Container } from '@material-ui/core';
import Card from './Card'
import { Route, withRouter } from 'react-router-dom'


class Sell extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    } 

    render() {

        const { classes } = this.props;
 
        return (
            <Container className={classes.mainContainer}>
                <Container className={classes.leftContainer} >
                    <Typography variant="h5" color="primary">Sell</Typography>
                    <Table className={classes.table}>
                        <TableHead>
                        <TableRow component={Button} disabled> 
                            <TableCell className={classes.cell}>Name</TableCell>
                            <TableCell className={classes.cell}>Description</TableCell>
                            <TableCell className={classes.cell}>Family</TableCell>
                            <TableCell className={classes.cell}>Affinity</TableCell>
                            <TableCell className={classes.cell}>Energy</TableCell>
                            <TableCell className={classes.cell}>HP</TableCell>
                            <TableCell className={classes.cell}>{`Price ($)`}</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                            {!!this.props.user.cardList ? this.props.user.cardList.map(card => 
                                <Card
                                    variant="line"
                                    action="sell"
                                    id={card.id}
                                    name={card.name}
                                    description={card.description}
                                    family={card.family}
                                    affinity={card.affinity}
                                    energy={card.energy}
                                    hp={card.hp}
                                    price ={card.price}
                                />
                            ):""}
                            {!!this.props.user.cardList ? this.props.user.cardList.length === 0 ? 
                                <Typography variant="h4" style={{textAlign: "center", marginTop: 20}}>No card</Typography>
                                : "" 
                                : ""
                            }
                        </TableBody>
                    </Table>
                </Container>
                <Container className={classes.rightContainer}>
                    <Container className={classes.cardContainer}>
                        <Route path="/sell/card/:id" render={ () =>
                            <Card 
                                variant="display"
                                action="sell"
                                location={this.props.location}
                            />
                        }/>
                    </Container>
                </Container>
            </Container>
        );

    }
}

const styles = theme => ({
    mainContainer: {
        flexGrow: 1,
        display:"flex",
        flexDirection:"row",
        padding:0,
    },
    leftContainer: {
        display:"flex",
        flexDirection:"column",
        flexGrow:1,
    },
    rightContainer: {
        display:"flex",
        flexDirection:"column",
        width:300,
    },
    table: {
        minWidth: 650,
    },
    cell: {
        textTransform: "capitalize",
        width: 100,
        textAlign: "center",
        fontWeight: "500",
        fontSize: 16,
    },
    cardContainer: {
        flexGrow: 1,
        width:300,
        display:"flex",
        flexDirection:"column",
        justifyContent: "center",
        alignItems: "middle",
        padding: 0,
    }
})

const mapStateToProps = (state) => {
  return {
      user: state.user,
  }
}

export default connect(mapStateToProps)(withRouter(withWidth()(withStyles(styles)(Sell))))