// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom'
import { addUser } from '../API/serverAPI';
import { _setUserId, _setUser, _setProfile } from '../Reducers/globalActions'

class Register extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            user: this.props.user,
            errorMessage: false,
            registered: false,
        }
    }
    
    handleEmail = event => {
        this.setState({
            user: {...this.state.user, email: event.target.value}
        })
    }

    handleSurName = event => {
        this.setState({
            user: {...this.state.user, surName: event.target.value}
        })
    }

    handleLastName = event => {
        this.setState({
            user: {...this.state.user, lastName: event.target.value}
        })
    }

    handleLogin = event => {
        this.setState({
            user: {...this.state.user, login: event.target.value}
        })
    }

    handlePwd = event => {
        this.setState({
            user: {...this.state.user, pwd: event.target.value}
        })
    }

    async registerFunction(){
        var responseAddUser = await addUser(this.state.user)
        if(responseAddUser.status === 200){
            this.setState({
                registered: true,
            })
        }
        else{
            this.setState({
                errorMessage: true,
            })
        }
    }

    render() {

        const { classes } = this.props;

        return (
            <div className={classes.paper}>
              <Container maxWidth="xs">
                <Typography variant="h5">
                 Sign up
                </Typography>
                <Grid container spacing={2}>
                <Grid item xs={12}>
                <TextField
                    autoFocus
                    margin="normal"
                    fullWidth
                    id="email"
                    label="Email Address"
                    name="email"
                    autoComplete="email"
                    onChange={this.handleEmail}
                />
                </Grid>    
                <Grid item xs={12}>
                <TextField
                    id="standard-lastname"
                    label="Last Name"
                    margin="normal"
                    fullWidth
                    onChange={this.handleLastName}

                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    id="standard-surname"
                    label="Surname"
                    margin="normal"
                    fullWidth
                    onChange={this.handleSurName}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    id="standard-login"
                    label="Login"
                    margin="normal"
                    fullWidth
                    onChange={this.handleLogin}
                />
                </Grid>
                <Grid item xs={12}>
                <TextField
                    id="standard-password-input"
                    label="Password"
                    type="password"
                    autoComplete="current-password"
                    margin="normal"
                    fullWidth
                    onChange={this.handlePwd}
                />
                </Grid>
                <Button
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={() => this.registerFunction()}
                >
                    Sign Up
                </Button>
                {this.state.errorMessage ? 
                (
                    <Typography variant="body1" color="error">Something went wrong</Typography>
                ) : (<div></div>)
                }
                {this.state.registered ? 
                (
                    <Typography variant="body1" color="textPrimary">Thank you for your registration ! You can now log in.</Typography>
                ) : (<div></div>)
                }
                <Grid container justify="flex-end">
                    <Grid item>
                    <Typography component={Link} to="/" variant="body2">Already have an account? Sign in</Typography>
                    </Grid>
                </Grid>
                </Grid>
                </Container>
            </div>
            
        );

    }
}

const styles = theme => ({
    mainContainer: {
        flexGrow: 1,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
});
 

const mapStateToProps = (state) => {
    return {
        user: state.user,
    }
}



export default connect(mapStateToProps)(withStyles(styles)(Register))