// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import  { Button } from '@material-ui/core';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import LocalAtmIcon from '@material-ui/icons/LocalAtm';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import SportsEsportsIcon from '@material-ui/icons/SportsEsports';

class Home extends React.Component {
    render() {

        const { classes } = this.props;

        return (
            <div className={classes.mainContainer}>
                <Button component={Link} to="/buy" variant="contained" startIcon={<ShoppingCartIcon/>} style={{backgroundColor: "#2DBEFF", color: "#FFF"}} className={classes.button}>Buy</Button>
                <Button component={Link} to="/sell" variant="contained" startIcon={<LocalAtmIcon/>} style={{backgroundColor: "#9EF769", color: "#000"}} className={classes.button}>Sell</Button> 
                <Button component={Link} to="/play" variant="contained" startIcon={<SportsEsportsIcon/>} style={{backgroundColor: "#054752", color: "#FFF"}} className={classes.button}>Play</Button>         
            </div>
        )
    }
}


const styles = theme => ({
    mainContainer: {
        flexGrow: 1,
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        justifyContent: "center",
    },
    button: {
        borderRadius: 80,
        width: 260,
        height: 150,
        margin: 8,
    },
})

const mapStateToProps = (state) => {
    return {
    }
}
  
export default connect(mapStateToProps)(withWidth()(withStyles(styles)(Home)))