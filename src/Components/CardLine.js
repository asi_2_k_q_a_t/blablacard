// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import {Button, TableCell, TableRow}  from '@material-ui/core';

class CardLine extends React.Component {

    constructor(props){
        super(props)
        this.state = {
        }
    }
    
    handleClick(){
        this.props.history.push(`/${this.props.action}/card/${this.props.id}`)
    }

    render() {

        const { classes } = this.props;

        return (
                <TableRow component={Button} onClick={() => this.handleClick()} className={classes.tableRow}>
                    <TableCell className={classes.cell}>{this.props.name}</TableCell>                       
                    <TableCell className={classes.cell}>{this.props.description}</TableCell>                     
                    <TableCell className={classes.cell}>{this.props.family}</TableCell>                    
                    <TableCell className={classes.cell}> {this.props.affinity} </TableCell>
                    <TableCell className={classes.cell}> {Math.round(this.props.energy)} </TableCell>                    
                    <TableCell className={classes.cell}> {Math.round(this.props.hp)} </TableCell>
                    <TableCell className={classes.cell}> {Math.round(this.props.price)} </TableCell>
                </TableRow>          
        );
    }
}

const styles = theme => ({
    mainContainer: {
        flexGrow: 1,
    },
    topCard: {
        display: "flex",
        flexDirection: "row",
        padding: 0,
    },
    cell: {
        textTransform: "capitalize",
        width: 100,
        textAlign: "center",
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    tableRow: {
        flexGrow: 1,
        flexDirection: "row",
    }
})

const mapStateToProps = (state) => {
  return {
      currentCard: state.currentCard,
  }
}

export default connect(mapStateToProps)(withRouter(withWidth()(withStyles(styles)(CardLine))))