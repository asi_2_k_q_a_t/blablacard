// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import { connect } from 'react-redux'
import { _setUserId, _setUser } from '../Reducers/globalActions'
import { Link } from 'react-router-dom'
import  { Typography, Button, TextField, Grid } from '@material-ui/core';
import { getAuth, getUser } from '../API/serverAPI'

class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            login: "",
            pwd: "",
            error: false,
        }
    }

    handleChangeLogin = event => {
        this.setState({
            login: event.target.value,
        })
    }

    handleChangePwd = event => {
        this.setState({
            pwd: event.target.value,
        })
    }

    async login(){
        var responseAuth = await getAuth(this.state.login, this.state.pwd)
        if(responseAuth.data !== -1){

            _setUserId(this.props, responseAuth.data)
            localStorage.setItem("token", responseAuth.data)

            var responseUser = await getUser(responseAuth.data)
            if(!!responseUser){
                _setUser(this.props, responseUser.data)
            }
        }
        else{
            this.setState({
                error: true,
            })
        }
    }

    render() {

        const { classes } = this.props;

        return (
            <div className={classes.mainContainer}>
                <Typography variant="h3" color="primary">Bla Bla Card</Typography>
                <Grid>
                    <TextField
                        id="login"
                        label="Username"
                        type="text"
                        name="login"
                        margin="normal"
                        variant="outlined"
                        value={this.state.login}
                        onChange={this.handleChangeLogin}
                        error={this.state.error}
                    />
                </Grid>
                <Grid>
                    <TextField
                        id="pwd"
                        label="Password"
                        type="password"
                        name="pwd"
                        margin="normal"
                        variant="outlined"
                        value={this.state.pwd}
                        onChange={this.handleChangePwd}
                        error={this.state.error}
                        onKeyPress={event => {
                            if (event.key === 'Enter') {
                                this.login()
                            }
                        }}
                    />
                </Grid>
                <Grid>
                    <Button variant="contained" color="primary" style={{marginTop: 8}} onClick={() => this.login()}>Sign in</Button>
                </Grid>

                <Typography className={this.state.error ? classes.error : classes.hide} variant="body1" color="error">Username or password is incorrect</Typography>

                <Typography variant="body2" style={{marginTop: 32}}>Don't have an account yet?</Typography>
                <Button component={Link} to="/register" variant="contained" color="secondary" style={{marginTop: 8}}>Subscribe</Button>
            </div>
        );

    }
}

const styles = theme => ({
    hide:{
        display: "none",
    },
    mainContainer: {
        flexGrow: 1,
        margin: "40%",
        marginTop: 100,
        [theme.breakpoints.down('xs')]: {
            margin: 8,
            marginTop: 50,
        },
    },
    error:{
        height: 60,
    },
})

const mapStateToProps = (state) => {
  return {
      userId: state.userId,
  }
}

export default connect(mapStateToProps)(withWidth()(withStyles(styles)(Login)))