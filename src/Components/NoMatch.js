// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import { BrowserRouter, Switch, Route, Link, Redirect } from 'react-router-dom'
import { Typography } from '@material-ui/core'

class NoMatch extends React.Component {

    render() {

        const { classes } = this.props;
        const errorText = this.props.errorCode === "404" ? "Page not found" :
            this.props.errorCode === "403" ? "Forbidden" : ""

        return (
            <div className={classes.noMatchContainer}>
                <div className={classes.msgContainer}>
                    <Typography style={{fontSize: 36, marginTop: "7%", marginLeft: "7%"}}>Bla Bla Card</Typography>
                    <Typography style={{fontSize: 150, marginTop: "7%", marginLeft: "7%"}}>{this.props.errorCode}</Typography>
                    <Typography style={{fontSize: 24, marginTop: -15, marginLeft: "8%"}}>{errorText}</Typography>
                </div> 
            </div>
        );

    }
}

const styles = theme => ({
    noMatchContainer:{
        position: "absolute",
        width: "100%",
        height: "100%",
        top: 0,
        left: 0,
        zIndex: 2000,
    },
    msgContainer:{
        width: "100%",
        height: "100%",
        backgroundColor: "#2dbeff",
        color: "#FFF",
        display: "flex",
        flexDirection: "column",
    },
})

export default withWidth()(withStyles(styles)(NoMatch))