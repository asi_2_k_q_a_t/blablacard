// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import { connect } from 'react-redux'
import { Button, Table, TableBody, TableCell, TableHead, TableRow, Typography, Container } from '@material-ui/core';
import Card from './Card'
import { Route, withRouter } from 'react-router-dom'
import { getCardsToSell } from '../API/serverAPI'
import { _setCardsToSell } from '../Reducers/globalActions'


class Buy extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    
    componentDidMount(){
        this.getCards(this.props)
    }

    componentWillReceiveProps(nextProps){
        if(!!nextProps.user.cardList && !!this.props.user.cardList){
            if(Object.keys(nextProps.user.cardList).length !== Object.keys(this.props.user.cardList).length){
                this.getCards(nextProps)
            }
        }
    }

    async getCards(props){
        var responseCards = await getCardsToSell();
        if(!!responseCards){
            _setCardsToSell(props, responseCards.data)
        }
    }

    render() {

        const { classes } = this.props;
 
        return (
            <Container className={classes.mainContainer}>
                <Container className={classes.leftContainer} >
                    <Typography variant="h5" color="primary">Buy</Typography>
                    <Table className={classes.table}>
                        <TableHead>
                        <TableRow component={Button} disabled> 
                            <TableCell className={classes.cell}>Name</TableCell>
                            <TableCell className={classes.cell}>Description</TableCell>
                            <TableCell className={classes.cell}>Family</TableCell>
                            <TableCell className={classes.cell}>Affinity</TableCell>
                            <TableCell className={classes.cell}>Energy</TableCell>
                            <TableCell className={classes.cell}>HP</TableCell>
                            <TableCell className={classes.cell}>{`Price ($)`}</TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                            {!!this.props.cardsToSell ? this.props.cardsToSell.map(card => 
                                <Card
                                    variant="line"
                                    action="buy"
                                    id={card.id}
                                    name={card.name}
                                    description={card.description}
                                    family={card.family}
                                    affinity={card.affinity}
                                    energy={card.energy}
                                    hp={card.hp}
                                    price ={card.price}
                                />
                            ):""}
                            {!!this.props.cardsToSell ? this.props.cardsToSell.length === 0 ? 
                                <Typography variant="h4" style={{textAlign: "center", marginTop: 20}}>No card</Typography>
                                : "" 
                                : ""
                            }
                        </TableBody>
                    </Table>
                </Container>
                <Container className={classes.rightContainer}>
                    <Container className={classes.cardContainer}>
                        <Route path="/buy/card/:id" render={ () =>
                            <Card 
                                variant="display"
                                action="buy"
                                location={this.props.location}
                            />
                        }/>
                    </Container>
                </Container>
            </Container>
        );

    }
}

const styles = theme => ({
    mainContainer: {
        flexGrow: 1,
        display:"flex",
        flexDirection:"row",
        padding:0,
    },
    leftContainer: {
        display:"flex",
        flexDirection:"column",
        flexGrow:1,
    },
    rightContainer: {
        display:"flex",
        flexDirection:"column",
        width:300,
    },
    table: {
        minWidth: 650,
    },
    cell: {
        textTransform: "capitalize",
        width: 100,
        textAlign: "center",
        fontWeight: "500",
        fontSize: 16,
    },
    cardContainer: {
        flexGrow: 1,
        width:300,
        display:"flex",
        flexDirection:"column",
        justifyContent: "center",
        alignItems: "middle",
        padding: 0,
    }
})

const mapStateToProps = (state) => {
  return {
      user: state.user,
      cardsToSell: state.cardsToSell,
  }
}

export default connect(mapStateToProps)(withRouter(withWidth()(withStyles(styles)(Buy))))