// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import { connect } from 'react-redux'
import socketIOClient from "socket.io-client";
import { _setUserId, _setUser, _setSocket } from '../Reducers/globalActions'
import { BrowserRouter, Switch, Route, Link, Redirect } from 'react-router-dom'
import { AppBar, Toolbar, Typography, Button, Drawer, List, ListItem, ListItemText, ListItemIcon, IconButton, Divider, Fade, CircularProgress } from '@material-ui/core'
import { Menu, DirectionsRun } from '@material-ui/icons'
import { getUser } from '../API/serverAPI'


import Home from './Home'
import Login from './Login'
import Buy from './Buy'
import Sell from './Sell'
import Play from './Play'
import Settings from './Settings'
import Register from './Register'
import NoMatch from './NoMatch'

class Main extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            drawer: false,
            spinning: true,
            loading: true,
        }
    }

    componentDidMount(){
        this.token()
        this.simulateLoading()

        var endpoint = "localhost:8080/"
        var socket = socketIOClient(endpoint)
        _setSocket(this.props, socket)
    }

    componentWillReceiveProps(nextProps){
        // Simulation d'un temps de chargement des données
        if(!!nextProps.userId && nextProps.userId !== this.props.userId){
            this.toggleLoading(true)
            this.toggleSpinning(true)
            clearTimeout(this.spinner)
            clearTimeout(this.loader)
            this.simulateLoading()
        }
    }

    toggleDrawer(value){
        this.setState({
            drawer: value,
        })
    }

    toggleSpinning(value){
        this.setState({
            spinning: value,
        })
    }

    toggleLoading(value){
        this.setState({
            loading: value,
        })
    }

    simulateLoading(){
        this.spinner = setTimeout(() => this.toggleSpinning(false), 600)
        this.loader = setTimeout(() => this.toggleLoading(false), 1000)
    }

    async token(){
        // Le token de session correspond à l'id utilisateur
        var token = localStorage.getItem("token")

        if(!!token && token !== "-1"){            
            _setUserId(this.props, token)

            var responseUser = await getUser(token)

            if(!!responseUser){
                _setUser(this.props, responseUser.data)
            }
        }
    }

    logout(){
        localStorage.setItem("token", -1)
        document.location.href = "/"
    }

    render() {

        const { classes } = this.props;

        return (

            <MuiThemeProvider theme={mainTheme}>
            <BrowserRouter>
                <div className={this.props.userId !== -1 && this.state.loading ? classes.loaderContainer : classes.hide}>
                    <Fade in={this.state.spinning} timeout={{enter: 0, exit: 400}}>
                        <div className={classes.spinnerContainer}>
                            <CircularProgress size="10em" className={this.state.spinning ? classes.spinner : classes.hide} color="inherit"/>
                        </div>                   
                    </Fade>
                </div>

                <div className={this.props.userId !== -1 ? classes.headerContainer : classes.hide}>
                    <AppBar>
                        <Toolbar className={classes.toolBar}>
                            <IconButton edge="start" onClick={() => this.toggleDrawer(true)} color="inherit">
                                <Menu/>
                            </IconButton>
                            <div className={classes.grow}>
                                {isWidthDown('xs', this.props.width) ? "" : <Button component={Link} to="/" color="inherit" className={classes.homeButton}>Bla Bla Card</Button>}
                            </div>
                            <Typography variant="body1" className={classes.headerText}>{`${Math.round(this.props.user.account)} $`}</Typography>
                        </Toolbar>
                    </AppBar>
                    <Drawer anchor="left" open={this.state.drawer} onClose={() => this.toggleDrawer(false)}>
                        <List className={classes.list}>
                            <ListItem>
                                <ListItemText 
                                    primary={`${this.props.user.login}`}
                                    secondary={`${this.props.user.surName} ${this.props.user.lastName}`}
                                />
                            </ListItem>
                            <Divider/>
                            <ListItem button component={Link} to="/" onClick={() => this.toggleDrawer(false)}>
                                <ListItemText primary="Home"/>
                            </ListItem>
                            <ListItem button component={Link} to="/buy" onClick={() => this.toggleDrawer(false)}>
                                <ListItemText primary="Buy"/>
                            </ListItem>
                            <ListItem button component={Link} to="/sell" onClick={() => this.toggleDrawer(false)}>
                                <ListItemText primary="Sell"/>
                            </ListItem>
                            <ListItem button component={Link} to="/play" onClick={() => this.toggleDrawer(false)}>
                                <ListItemText primary="Play"/>
                            </ListItem>
                            <ListItem button component={Link} to="/settings" onClick={() => this.toggleDrawer(false)}>
                                <ListItemText primary="Settings"/>
                            </ListItem>

                            <div style={{flexGrow: 1}}></div>

                            <ListItem button onClick={() => this.logout()}>
                            <ListItemIcon>
                                <DirectionsRun/>
                            </ListItemIcon>
                                <ListItemText primary="Log out"/>
                            </ListItem>
                        </List>
                    </Drawer>
                </div>
                <div className={classes.mainContainer}>
                    <Toolbar className={this.props.userId !== -1 ? classes.toolBar : classes.hide}/>
                    <Switch>
                        <Route exact path="/" render={this.props.userId !== -1 ? () => <Home/> : () => <Login/>}/>
                        <Route path="/register" render={this.props.userId !== -1 ? () => <NoMatch errorCode="403"/> : () => <Register/>} />
                        <Route path="/settings" render={this.props.userId !== -1 ? () => <Settings/> : () => <NoMatch errorCode="403"/>} />
                        <Route path="/play" render={this.props.userId !== -1 ? () => <Play/> : () => <NoMatch errorCode="403"/>} />
                        <Route path="/buy" render={this.props.userId !== -1 ? () => <Buy/> : () => <NoMatch errorCode="403"/>} />
                        <Route path="/sell" render={this.props.userId !== -1 ? () => <Sell/> : () => <NoMatch errorCode="403"/>} />
                        <Route path="*" render={() => <NoMatch errorCode="404"/>} />
                    </Switch>
                </div>
            </BrowserRouter>
            </MuiThemeProvider>
        );

    }
}

const styles = theme => ({
    hide:{
        display: "none",
    },
    headerContainer:{
        flexGrow: 1,
    },
    loaderContainer:{
        position: "absolute",
        width: "100%",
        height: "100%",
        top: 0,
        left: 0,
        zIndex: 3000,
    },
    spinnerContainer:{
        width: "100%",
        height: "100%",
        backgroundColor: "#2dbeff",
        color: "#FFF",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
    spinner:{
        display: "block",
    },
    mainContainer:{
        position: "absolute",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        padding: 8,
        display: "flex",
        flexDirection: "column",
    },
    toolBar:{
        diplay: "flex",
        flexDirection: "row",
    },
    grow:{
        flexGrow: 1,
        marginLeft: 70, 
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
    },
    list:{
        width: 250,
        height: "95%",
        display: "flex",
        flexDirection: "column",
    },
    headerText:{
        width: 100,
        textAlign: "right",
    },
    homeButton:{
        width: 200,
        fontSize: 28,
        textTransform: "none",
    }
})

const mainTheme = createMuiTheme({
    palette: {
        primary: { main: "#2dbeff" },
        secondary: { main: "#9EF769" },
    },
    typography:{
        fontFamily: "'Reem Kufi', sans-serif",
    }
});

const mapStateToProps = (state) => {
  return {
      userId: state.userId,
      user: state.user,
  }
}

export default connect(mapStateToProps)(withWidth()(withStyles(styles)(Main)))