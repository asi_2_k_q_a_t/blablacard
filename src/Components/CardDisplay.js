// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Card, CardContent, Container, Typography, Button } from '@material-ui/core';
import { Close } from '@material-ui/icons'
import { sellCard, buyCard, getUser } from '../API/serverAPI'
import { _setUser } from '../Reducers/globalActions'

class CardDisplay extends React.Component {

    constructor(props) {
        super(props)
        this.state = {          
        }
    }
    
    async sellingCard(){
        var order = {
            user_id: this.props.userId,
            card_id: this.props.id
        }
        var responseSellCard = await sellCard(order)

        if(responseSellCard.data){
            var responseGetUser = await getUser(this.props.userId);
            if(!!responseGetUser){
                _setUser(this.props, responseGetUser.data);
            }
        }
    }

    async buyingCard(){
        var order = {
            user_id: this.props.userId,
            card_id: this.props.id
        }
        var responseBuyCard = await buyCard(order)

        if(responseBuyCard.data){
            var responseGetUser = await getUser(this.props.userId);
            if(!!responseGetUser){
                _setUser(this.props, responseGetUser.data);
            }
        }
    }

    checkCard(){
        if(this.props.action === "sell"){
            return !!this.props.user.cardList && this.props.user.cardList.findIndex(card => card.id === this.props.id)
        }
        else if(this.props.action === "buy"){
            return !!this.props.cardsToSell && this.props.cardsToSell.findIndex(card => card.id === this.props.id)
        }
    }

    render() {

        const { classes } = this.props;

        return (
            <div className={this.checkCard() !== -1 ? classes.mainContainer : classes.hide}>
                <Card className={classes.card}>
                    <CardContent>
                        <Container className={classes.topCard}>
                            <img 
                                className={classes.logo}
                                src="http://cdn.shopify.com/s/files/1/0007/0139/9158/products/tatouage_eclair_600x.png?v=1557933088"
                                title="Energy logo"
                            />
                            <Typography className={classes.number}> {Math.round(this.props.energy)} </Typography>
                            <Typography className={classes.title} color="textSecondary" gutterBottom>
                                {this.props.family}
                            </Typography>
                            <Typography className={classes.number}> {Math.round(this.props.hp)} </Typography>
                            <img 
                                className={classes.logo}
                                src="https://img-19.ccm2.net/1PBNp2xZFDPrr3tx9-GPqLxNpK4=/500x/2589beddf4bf400aa490286984009183/ccm-ugc/coeur-blanc_318-10387.jpg"
                                title="HP logo"
                            />
                        </Container>
                        <Typography variant="h5">
                            {this.props.name}
                        </Typography>
                        <img
                            className={classes.media}
                            //src="https://www.themadeco.fr/67325-thickbox_default/disque-sucre-licorne.jpg"
                            src={this.props.imgUrl}
                            title="Image representing the card"
                        />
                        <Typography variant="body1">
                            {this.props.description}
                        </Typography>
                        <Typography variant="h6" style={{marginTop: 20}}>
                            {`Price: ${Math.round(this.props.price)} $`}
                        </Typography>
                    </CardContent>
                </Card>
                <Button variant="contained" color="primary" style={{marginTop: 8}} onClick={
                    this.props.action === "sell" ? () => this.sellingCard()
                    : this.props.action === "buy" ? () => this.buyingCard()
                    : () => {} 
                } fullWidth>
                    {
                        this.props.action === "sell" ? "Sell"
                        : this.props.action === "buy" ? "Buy"
                        : "" 
                    }
                </Button>
                <Button variant="contained" style={{marginTop: 8}} component={Link} to={`/${this.props.action}`} fullWidth>
                    Close
                </Button>            
            </div>
        );
    }
}

const styles = theme => ({
    hide:{
        display: "none",
    },
    mainContainer: {
        flexGrow: 1,
    },
    card: {
        minWidth: 275,
        height: 350,
    },
    topCard: {
        display: "flex",
        flexDirection: "row",
        padding: 0,
    },
    number: {
        width: 60,
        textAlign: "center",
    },
    logo: {
        height: 30,
    },
    media: {
        height: 100,
        margin: 16,
    },
    title: {
        textAlign: "center",
        flexGrow: 1,
    },
})

const mapStateToProps = (state) => {
  return {
      userId: state.userId,
      user: state.user,
      cardsToSell: state.cardsToSell,
  }
}

export default connect(mapStateToProps)(withWidth()(withStyles(styles)(CardDisplay)))