// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';
import { connect } from 'react-redux'

import Chat from './Chat'
import { Typography, Fade, CircularProgress } from '@material-ui/core'

class Play extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            spinning: true,
            stage: 0, // 0 - Loading / 1 - Picking cards / 2 - Fighting / 3 - Results
        };
    }

    componentDidMount = () => {
        if(!!this.props.user.id){
            this.joinGame(this.props)
        }
        this.props.socket.on('fight', msg => this.checkFight(msg))
    }

    componentWillReceiveProps(nextProps){
        if(!!nextProps.user.id && nextProps.user.id !== this.props.user.id){
            this.joinGame(nextProps)
        }
    }

    componentWillUnmount(){
        const user = {
            id: this.props.user.id,
            login: this.props.user.login
        }
        this.props.socket.emit('leave', user)
    }

    joinGame(props){
        const user = {
            id: props.user.id,
            login: props.user.login
        }
        this.props.socket.emit('join', user)
    }

    checkFight(msg){
        if(msg.user1.id === this.props.user.id || msg.user2.id === this.props.user.id){
            this.setState({
                spinning: false,
            })
            setTimeout(() => this.handleChangeStage(1), 400)
        }
    }

    handleChangeStage(value){
        this.setState({
            stage: value,
        })
    }

    render() {

        const { classes } = this.props;

        return (

        <div className={classes.mainContainer}>
            <div className={classes.chatContainer}>
                <Chat/>
                <div className={this.state.stage === 0 ? classes.loaderContainer : classes.hide}>
                    <Fade in={this.state.spinning} timeout={{enter: 0, exit: 0}}>
                        <div className={classes.spinnerContainer}>
                            <CircularProgress size="10em" className={this.state.spinning ? classes.spinner : classes.hide} color="inherit"/>
                            <Typography variant="h4" style={{marginTop: 8}}>Looking up for players...</Typography>
                        </div>                   
                    </Fade>
                </div>
            </div>
            <div className={classes.gameContainer}>
                <img 
                    style={{width: "50%", height: "50%", opacity: 0.2}}
                    src="http://cdn.shopify.com/s/files/1/0007/0139/9158/products/tatouage_eclair_600x.png?v=1557933088"
                    title="Energy logo"
                />
            </div>
        </div>

        );

    }
}

const styles = theme => ({
    hide:{
        display: "none",
    },
    mainContainer:{
        display: "flex",
        flexDirection: "row",
        flexGrow: 1,
        width: "100%",
        height: "100%",
    },
    loaderContainer:{
        position: "absolute",
        width: "100%",
        height: "100%",
        top: 0,
        left: 0,
        zIndex: 2000,
    },
    spinnerContainer:{
        width: "100%",
        height: "100%",
        backgroundColor: "#2dbeff64",
        color: "#FFF",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
    spinner:{
        display: "block",
    },
    chatContainer:{
        display: "flex",
        flexDirection: "row",
        width: 300,
    },
    gameContainer:{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        flexGrow: 1,
    },
})

const mapStateToProps = (state) => {
    return {
        socket: state.socket,
        user: state.user,
    }
}

export default connect(mapStateToProps)(withWidth()(withStyles(styles)(Play)))