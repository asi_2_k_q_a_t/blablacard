// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import { connect } from 'react-redux'

// Local imports
import CardLine from './CardLine';
import CardDisplay from './CardDisplay';

import propTypes from 'prop-types'
import { getCard } from '../API/serverAPI';

class Card extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            id: "",
            name: "",
            description: "",
            family: "",
            affinity: "",
            energy: "",
            hp: "",
            price: "",
            imgUrl: "",
        }
    }
    
    componentDidMount(){
        if (this.props.variant === "display"){
            this.getCardInfo(this.props)
        }
    }

    componentWillReceiveProps(nextProps){
        if(!!nextProps.location && nextProps.location !== this.props.location){
            this.getCardInfo(nextProps)
        }
    }

    async getCardInfo(props){
        const cardPath = props.location.pathname.split('/')
        const cardId = cardPath[cardPath.length - 1]

        var responseCard = await getCard(cardId) ;
        if(!!responseCard){
            this.setState({
                id: responseCard.data.id,
                name: responseCard.data.name,
                description: responseCard.data.description,
                family: responseCard.data.family,
                affinity: responseCard.data.affinity,
                energy: responseCard.data.energy,
                hp: responseCard.data.hp,
                price: responseCard.data.price,
                imgUrl: responseCard.data.imgUrl,
            })
        }
    }

    render() {

        const { classes } = this.props;

        return (
            <div className={classes.mainContainer}>
                {
                    this.props.variant === "line" ? 
                        <CardLine
                            id={this.props.id}
                            action={this.props.action}
                            name={this.props.name}
                            description={this.props.description}
                            family={this.props.family}
                            affinity={this.props.affinity}
                            energy={this.props.energy}
                            hp={this.props.hp}
                            price ={this.props.price}
                        />
                    : this.props.variant === "display" ?
                        <CardDisplay
                            id={this.state.id}
                            action={this.props.action}
                            location={this.props.location}
                            name={this.state.name}
                            description={this.state.description}
                            family={this.state.family}
                            affinity={this.state.affinity}
                            energy={this.state.energy}
                            hp={this.state.hp}
                            price={this.state.price}
                            imgUrl={this.state.imgUrl}
                        />
                    : ""
                }            
            </div>
        );
    }
}

const styles = theme => ({
    mainContainer: {
        flexGrow: 1,
    },
})

const mapStateToProps = (state) => {
  return {
      user: state.user,
  }
}

export default connect(mapStateToProps)(withWidth()(withStyles(styles)(Card)))


