// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux'
import { Container, Typography, Grid, Button, TextField, Snackbar, IconButton } from '@material-ui/core';
import { Close } from '@material-ui/icons'
import { putUser, getUser } from '../API/serverAPI'
import { _setUserId, _setUser, _setProfile } from '../Reducers/globalActions'

class Settings extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            email: "",
            surName: "",
            lastName: "",
            pwd: "",
            pwdConfirm: "",
            settingsChanged: false,
            error: false,
            errorPwd: false,
            errorNotMatch: false,
            snackOpen: false,
            snackMsg: "",
        }
    }

    componentDidMount(){
        if(!!this.props.user && Object.keys(this.props.user).length !== 0){
            this.setInputs(this.props.user)
        }
    }

    componentWillReceiveProps(nextProps){
        if(!!nextProps.user && Object.keys(nextProps.user).length !== 0){
            this.setInputs(nextProps.user)
        }
    }

    setInputs(user){
        const { email, surName, lastName } = user
        this.setState({
            email: email,
            surName: surName,
            lastName: lastName,
        })
    }
    
    handleChangeEmail = event => {
        this.setState({
            email: event.target.value,
            error: false,
        })
    }

    handleChangeSurName = event => {
        this.setState({
            surName: event.target.value,
            error: false,
        })
    }

    handleChangeLastName = event => {
        this.setState({
            lastName: event.target.value,
            error: false,
        })
    }

    handleCloseSnack(){
        this.setState({
            snackOpen: false,
        })
    }


    handleNewPwd = event => {
        this.setState({
            pwd: event.target.value,
            errorPwd: false,
            errorNotMatch: false,
        })
    }

    handleNewPwdConf = event => {
        this.setState({
            pwdConfirm: event.target.value,
            errorPwd: false,
            errorNotMatch: false,
        })
    }


    async saveChanges(){
        var user = {
            ...this.props.user,
            email: this.state.email,
            surName: this.state.surName,
            lastName: this.state.lastName,
        }

        var responsePutUser = await putUser(user)

        if(responsePutUser.status === 200){

            this.setState({
                snackOpen: true,
                snackMsg: "Your settings have been changed successfully!"
            })

            var responseGetUser = await getUser(this.props.userId);
            if(!!responseGetUser){
                _setUser(this.props, responseGetUser.data);
            }
        }
        else{
            this.setState({
                error: true,
            })
        }
    }

    async saveNewPwd(){
        if(this.state.pwdConfirm === this.state.pwd){

            var user = {
                ...this.props.user,
                pwd: this.state.pwd
            }

            var responsePutUser = await putUser(user)
            if(responsePutUser.status === 200){

                this.setState({
                    snackOpen: true,
                    snackMsg: "Your password has been changed!"
                })

                var responseGetUser = await getUser(this.props.userId);
                if(!!responseGetUser){
                    _setUser(this.props, responseGetUser.data);
                }
            }
            else{
                this.setState({
                    errorPwd: true,
                })
            }
        }
        else{
            this.setState({
                errorNotMatch: true,
            })
        }  
    }

    render() {

        const { classes } = this.props;

        return (
            <Container className={classes.mainContainer}>
                <Container className={classes.leftContainer}>
                <Typography variant="h5" color="primary">Settings</Typography>
                    <Grid>
                        <TextField
                            id="email"
                            label="Email"
                            type="email"
                            name="email"
                            margin="normal"
                            variant="outlined"
                            value={this.state.email}
                            error={this.state.error}
                            onChange={this.handleChangeEmail}
                        />
                    </Grid>
                    <Grid>
                        <TextField
                            id="surName"
                            label="First name"
                            type="text"
                            name="surName"
                            margin="normal"
                            variant="outlined"
                            value={this.state.surName}
                            error={this.state.error}
                            onChange={this.handleChangeSurName}
                        />
                    </Grid>
                    <Grid>
                        <TextField
                            id="lastName"
                            label="Last name"
                            type="text"
                            name="lastName"
                            margin="normal"
                            variant="outlined"
                            value={this.state.lastName}
                            error={this.state.error}
                            onChange={this.handleChangeLastName}
                        />
                    </Grid>
                    <Grid>
                        {this.state.error ? <Typography variant="body1" color="error">Something went wrong</Typography> : ""}
                        <Button variant="contained" color="primary" style={{marginTop: 8}} onClick={() => this.saveChanges()}>Save changes</Button>
                    </Grid>
                    <Typography variant="h5" color="primary" style={{marginTop: 32}}>Change password</Typography>
                    <Grid>
                        <TextField
                            id="new_password"
                            label="New password"
                            type="password"
                            name="new_password"
                            margin="normal"
                            variant="outlined"
                            value={this.state.pwd}
                            onChange={this.handleNewPwd}
                            error={this.state.errorPwd || this.state.errorNotMatch}
                        />
                    </Grid>
                    <Grid>
                        <TextField
                            id="confirm_password"
                            label="Confirm password"
                            type="password"
                            name="confirm_password"
                            margin="normal"
                            variant="outlined"
                            value={this.state.pwdConfirm}
                            onChange={this.handleNewPwdConf}
                            error={this.state.errorPwd || this.state.errorNotMatch}
                        />
                    </Grid>
                    <Grid>
                        {
                            this.state.errorNotMatch ? <Typography variant="body1" color="error">The passwords do not match.</Typography> :
                            this.state.errorPwd ? <Typography variant="body1" color="error">Something went wrong</Typography> : ""
                        }
                        <Button variant="contained" color="primary" style={{marginTop: 8}} onClick={() => this.saveNewPwd()}>Change password</Button>
                    </Grid>
                    <Snackbar
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'left',
                        }}
                        open={this.state.snackOpen}
                        autoHideDuration={6000}
                        onClose={() => this.handleCloseSnack()}
                        message={this.state.snackMsg}
                        action={[
                            <IconButton
                                key="close"
                                aria-label="close"
                                color="inherit"
                                onClick={() => this.handleCloseSnack()}
                            >
                                <Close/>
                            </IconButton>,
                        ]}
                    />
                </Container>
            </Container>
        );

    }
}

const styles = theme => ({
    mainContainer: {
        flexGrow: 1,
        display:"flex",
        flexDirection:"row",
        padding:0,
    },
    leftContainer: {
        display:"flex",
        flexDirection:"column",
        flexGrow:1,
    },
})

const mapStateToProps = (state) => {
  return {
      user: state.user,
      userId: state.userId,
  }
}

export default connect(mapStateToProps)(withStyles(styles)(Settings))