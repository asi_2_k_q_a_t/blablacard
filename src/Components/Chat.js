// Public imports
import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';
import { connect } from 'react-redux'
import { Typography, TextField, Paper, IconButton } from '@material-ui/core';
import { Send } from '@material-ui/icons';

class Chat extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            msg: [],
            userChat: undefined,
            userLeft: false,
            input: "",
        };
    }

    componentDidMount = () => {
        this.props.socket.on('message', msg => this.receive(msg))
        this.props.socket.on('fight', msg => this.checkFight(msg))
        this.props.socket.on('leave', socketId => this.checkLeave(socketId))
    }

    checkFight(msg){
        if(msg.user1.id === this.props.user.id){
            this.setState({
                userChat: msg.user2,
            })
        }
        else if(msg.user2.id === this.props.user.id){
            this.setState({
                userChat: msg.user1,
            })
        }
    }

    checkLeave(socketId){
        if(!!this.state.userChat && socketId === this.state.userChat.socketId){
            this.setState({
                userLeft: true
            })
        }
    }

    msgAutoScroll(){
        if(!!this.msgDiv){
            this.msgDiv.scrollTop = this.msgDiv.scrollHeight
        }
    }

    send(){
        const user = {id: this.props.user.id, login: this.props.user.login}
        this.props.socket.emit('message', {userChat: user, content: this.state.input})
        this.setState({
            msg: [...this.state.msg, {userChat: user, content: this.state.input}],
            input: "",
        })
        setTimeout(() => this.msgAutoScroll(), 50)
    }

    receive(msg){
        this.setState({
            msg: [...this.state.msg, msg],
        })
        setTimeout(() => this.msgAutoScroll(), 50)
    }

    handleChangeMsg= event => {
        this.setState({
            input: event.target.value,
        })
    }

    render() {

        const { classes } = this.props;
        const { response } = this.state;

        return (

        <div className={classes.mainContainer}>
            <Paper className={classes.chat}>
                    <div className={classes.messages} ref={(ref) => this.msgDiv = ref}>

                        {!!this.state.userChat && this.state.userChat.id !== -1 ?
                            <div style={{display: "flex", flexDirection: "row"}}>
                                <Paper style={{
                                    display: "inline-block",
                                    maxWidth: 265,
                                    backgroundColor: "#ef0474",
                                    color: "#FFF",
                                    overflowWrap: "break-word",
                                    margin: 8,
                                    padding: 9
                                }}>
                                    <Typography style={{fontStyle: "italic"}}>{`${this.state.userChat.login} joined the game`}</Typography>
                                </Paper>
                            </div> : ""
                        }

                        {this.state.msg.map(msg =>

                            msg.userChat.id === this.props.user.id ?

                            <div style={{display: "flex", flexDirection: "row"}}>
                                <div style={{flexGrow: 1}}></div>
                                <Paper style={{
                                    display: "inline-block",
                                    maxWidth: 250,
                                    backgroundColor: "#2dbeff",
                                    color: "#FFF",
                                    overflowWrap: "break-word",
                                    margin: 8,
                                    padding: 9
                                }}>
                                    <Typography>{msg.content}</Typography>
                                </Paper>
                            </div> :

                            msg.userChat.id === this.state.userChat.id ?

                            <div style={{display: "flex", flexDirection: "row"}}>
                                <Paper style={{
                                    display: "inline-block",
                                    maxWidth: 250,
                                    overflowWrap: "break-word",
                                    margin: 8,
                                    padding: 9
                                }}>
                                    <Typography>{msg.content}</Typography>
                                </Paper>
                            </div> : ""
                        )}

                        {!!this.state.userChat && this.state.userChat.id !== -1 && this.state.userLeft ?
                            <div style={{display: "flex", flexDirection: "row"}}>
                                <Paper style={{
                                    display: "inline-block",
                                    maxWidth: 250,
                                    backgroundColor: "#ef0474",
                                    color: "#FFF",
                                    overflowWrap: "break-word",
                                    margin: 8,
                                    padding: 9
                                }}>
                                    <Typography style={{fontStyle: "italic"}}>{`${this.state.userChat.login} left the game`}</Typography>
                                </Paper>
                            </div> : ""
                        }

                    </div>
                    <Paper className={classes.entry}>
                        <TextField
                            id="Message"
                            placeholder="Type your message..."
                            type="text"
                            name="message"
                            margin="normal"
                            variant="outlined"
                            color="secondary"    
                            value={this.state.input}
                            onChange={this.handleChangeMsg}
                            error={this.state.error}
                            inputProps={{style: {height: 15, backgroundColor: "#FFF", borderRadius: 4}}}                           
                            onKeyPress={event => {
                                if (event.key === 'Enter') {
                                    this.send()
                                }
                            }}
                        />
                        <IconButton style={{margin: 5}} aria-label="send" onClick={() => this.send()}>
                            <Send/>
                        </IconButton>
                    </Paper>
            </Paper>
        </div>

        );

    }
}

const styles = theme => ({
    mainContainer:{
        display: "flex",
        flexDirection: "column",
        flexGrow: 1,
    },
    chat:{
        display: "flex",
        flexDirection: "column",
        width: 300,
        flexGrow: 1,
    },
    messages: {
        height: 0,
        overflowY: "scroll",
        flexGrow: 1,
    },
    entry:{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        height: 75,
        backgroundColor: "#efefef",
    },
})

const mapStateToProps = (state) => {
  return {
      socket: state.socket,
      user: state.user,
  }
}

export default connect(mapStateToProps)(withWidth()(withStyles(styles)(Chat)))