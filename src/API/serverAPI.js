var axios = require("axios")

export async function getAuth(login, pwd) {
    try {
        const response = await axios({
            method: "get",
            url: "http://localhost:9000/auth",
            responseType: "json",
            params: {
                login: login,
                pwd: pwd,
            },
        });
        return response;
    } catch (error) {
        return error;
    }
}

export async function getUser(userId) {
    try {
        const response = await axios({
            method: "get",
            url: "http://localhost:9000/user/" + userId,
            responseType: "json",
        });
        return response;
    } catch (error) {
        return error;
    }
}

export async function putUser(user) {
    const {id, lastName, surName, pwd, email, login, account} = user
    try {
        const response = await axios({
            method: "put",
            url: "http://localhost:9000/user/" + id,
            responseType: "json",
            data: {
                lastName: lastName,
                surName: surName,
                pwd: pwd,
                email: email,
                login: login,
                account: account,
            }
        });
        return response;
    } catch (error) {
        return error;
    }
}

export async function addUser(user) {
    const {lastName, surName, pwd, email, login} = user
    try {
        const response = await axios({
            method: "post",
            url: "http://localhost:9000/user/",
            responseType: "json",
            data: {
                lastName: lastName,
                surName: surName,
                pwd: pwd,
                email: email,
                login: login,
            }
        });
        return response;
    } catch (error) {
        return error;
    }
}

export async function getCard(cardId) {	
    try {
        const response = await axios({
            method: "get",
            url: "http://localhost:9000/card/" + cardId,
            responseType: "json",
        });
        return response;
    } catch (error) {
        return error;
    }
}

export async function sellCard(order) {
    const {user_id, card_id} = order
    try {
        const response = await axios({
            method: "post",
            url: "http://localhost:9000/sell/",
            responseType: "json", //it is supposed to be true or false, so line to delete?
            data: {
                user_id: user_id,
                card_id: card_id,
            }
        });
        return response;
    } catch (error) {
        return error;
    }
}

export async function buyCard(order) {
    const {user_id, card_id} = order
    try {
        const response = await axios({
            method: "post",
            url: "http://localhost:9000/buy/",
            responseType: "json", //it is supposed to be true or false, so line to delete?
            data: {
                user_id: user_id,
                card_id: card_id,
            }
        });
        return response;
    } catch (error) {
        return error;
    }
}

export async function getCardsToSell(){
    try {
        const response = await axios({
            method: "get",
            url: "http://localhost:9000/cards_to_sell",
            responseType: "json",
        });
        return response;
    } catch (error) {
        return error;
    }  
}